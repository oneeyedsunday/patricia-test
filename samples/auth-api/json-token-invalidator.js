'use strict';

// ideally this would be backed by redis + TTL
// or an in memory cache that expires keys for our purposes
const invalidatedTokens = new Set();

const JsonTokenInvalidator = {
  /**
     * Invalidate a token
     * @param {string} token
     */
  invalidateToken(token) {
    invalidatedTokens.add(token);
  },
  /**
     *
     * @param {string} token
     * @returns {boolean}
     */
  isTokenValid(token) {
    return !invalidatedTokens.has(token);
  }
};

module.exports = JsonTokenInvalidator;
