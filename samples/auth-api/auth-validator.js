'use strict';

const Joi = require('joi');

const loginValidator = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
  password: Joi.string().min(6).required()
});

const registerValidator = Joi.object().keys({
  first_name: Joi.string().min(3).required(),
  last_name: Joi.string().min(3).required(),
  email: Joi.string().email().required(),
  phone: Joi.string().length(11),
  password: Joi.string().min(6).required()
});

const validateRegister = registerValidator.validateAsync.bind(registerValidator);

const validateLogin = loginValidator.validateAsync.bind(loginValidator);

module.exports = {
  validateRegister,
  validateLogin
};
