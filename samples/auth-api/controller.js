'use strict';

const AuthService = require('./service');
const JsonTokenInvalidator = require('./json-token-invalidator');

function handleSuccess(res, message, object = null) {
  return res
    .sendJson(200, { status: 200, error: false, message, body: object });
}

function handleError(error, res, message, errorLogger) {
  errorLogger(error.message || message, { error });
  if (error.name === 'ValidationError') {
    return res.sendJson(422, {
      status: 422,
      error: true,
      message: 'Validation Error: ' + error.message
    });
  }


  // make sure its a valid HTTP Status Code...
  error.code = typeof error.code === 'number' && error.code > 100 && error.code < 600 ? error.code : 500;

  return res.sendJson(error.code, {
    status: error.code,
    error: true,
    message: error.code >= 500 ? 'System Error: ' + message : message
  });
};

module.exports = (logger) => ({
  login(req, res) {
    AuthService
      .login(req.body)
      .then(token => handleSuccess(res, 'Login successful', token))
      .catch(err => handleError(err, res, err.message, logger.error));
  },

  logout(req, res) {
    JsonTokenInvalidator.invalidateToken(req.headers['authorization']);

    return handleSuccess(res, 'Log out successful');
  },

  register(req, res) {
    AuthService
      .register(req.body)
      .then(token => handleSuccess(res, 'Registration successful', token))
      .catch(err => handleError(err, res, err.message, console.error));
  }
});
