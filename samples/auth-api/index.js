'use strict';

const jwt = require('jsonwebtoken');

const Framework = require('../../src');

const { SECRET_KEY } = require('./config');
const { createJWT } = require('./token-generator');
const AuthController = require('./controller')(console);

const app = Framework({ port: 8002 });

const authGuardedRoutes = [
  '/api/v1/auth/logout'
];

app.applyMiddleware(async(req, res, next) => {
  if (!authGuardedRoutes.includes(req.url)) return next();
  if (!req.headers.authorization) {
    return res.sendJson(401, {
      status: 401,
      error: true,
      message: 'Please make sure your request has an Authorization header'
    });
  }

  const token = req.headers.authorization;
  try {
    const decoded = await jwt.verify(token, SECRET_KEY);

    // eslint-disable-next-line no-unused-vars
    const { exp, iat, ...user } = decoded;

    req.user = user;

    if (req.url !== '/api/v1/auth/logout') {
      const refreshedToken = createJWT(decoded);
      res.set('Authorization', refreshedToken);
    }


    return next();
  } catch (err) {
    return res.sendJson(401, {
      status: 401,
      error: true,
      message: 'Failed to authenticate token'
    });
  }
});

app.router().post('/api/v1/auth/register', AuthController.register);
app.router().post('/api/v1/auth/login', AuthController.login);
app.router().post('/api/v1/auth/logout', AuthController.logout);
