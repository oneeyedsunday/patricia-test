'use strict';


const bcrypt = require('bcryptjs');
const UserRepo = require('./user-repository');
const { validateLogin, validateRegister } = require('./auth-validator');

const { createJWT } = require('./token-generator');


/**
 * throws error
 *
 * @param {number} code - The HTTP response code to send
 * @param {string} message - The error message to send
 * @throws {Error<message>} Throws an application error
 */
const throwError = (code, message) => {
  let err = new Error(message);
  err.code = code;
  throw err;
};


class AuthService {
  constructor(repository) {
    /**
     * @type {import('./irepository')}
     */
    this._repository = repository;
  }

  async login({ email, password } = {}) {
    await validateLogin({ email, password });
    const userMatches = this._repository.find({ email });

    if (!userMatches.length) throwError(401, 'Invalid credentials');
    const [{ password: hashedPassword, ...user }] = userMatches;
    const passwordMatch = await bcrypt.compare(password, hashedPassword);


    if (!passwordMatch) {
      throwError(401, 'Password mismatch');
    }

    return {
      user,
      token: createJWT({ email })
    };
  }

  async register(data = {}) {
    await validateRegister(data);
    const { email, password } = data;
    const existingUser = this._repository.find({ email });
    if (existingUser && existingUser[0]) {
      return throwError(409, 'Email taken');
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    this._repository.insert({
      email,
      password: hashedPassword
    });

    return {
      token: createJWT({ email })
    };
  }
}
const singletonInstance = new AuthService(new UserRepo());

module.exports = singletonInstance;
