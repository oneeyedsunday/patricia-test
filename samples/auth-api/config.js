'use strict';

module.exports = {
  SECRET_KEY: require('crypto').randomBytes(1024)
};
