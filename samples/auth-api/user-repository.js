'use strict';

const IRepository = require('./irepository');

const schema = [
  'email', 'id'
];

const users = [];

class UserRepository extends IRepository {
  findAll() {
    return users.slice();
  }

  findBydId(id) {
    const match = users.find(u => u.id === id);
    if (!match) return null;
    return Object.assign({}, match);
  }

  find(query = {}) {
    const filterKeys = Object.keys(query)
      .filter(queryFilter => schema.includes(queryFilter));

    return users.filter(u => {
      let match = true;

      filterKeys.forEach(filterProp => {
        match &= (u[filterProp] === query[filterProp]);
      });

      return match;
    })
      .slice();
  }

  insert(user) {
    users.push(user);
  }
}

module.exports = UserRepository;
