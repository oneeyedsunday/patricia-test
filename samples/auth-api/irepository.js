'use strict';

class IRepository {
  findAll() {
    throw new Error('Implement in base class');
  }

  findById(id) {
    throw new Error('Implement in base class');
  }

  find(query) {
    throw new Error('Implement in base class');
  }

  insert(item) {
    throw new Error('Implement in base class');
  }
}

module.exports = IRepository;
