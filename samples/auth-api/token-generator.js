'use strict';

const jwt = require('jsonwebtoken');
const { SECRET_KEY } = require('./config');

const createJWT = model => {
  return jwt.sign(model, SECRET_KEY, {
    expiresIn: 60 * 60 * 24 // expires in a day 24 hours
  });
};

module.exports = {
  createJWT
};
