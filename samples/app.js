'use strict';

const Framework = require('../src');

const app = Framework({ port: 9999 });

app.applyMiddleware((req, res, next) => {
  if (req.url === '/bash') {
    console.log(req.url);
  }

  next();
});

app.applyErrorHandler((err, req, res, next) => {
  console.error(err.name, err.message);
  res.sendJson(500, { message: 'Application is down.' });
});

app.router().get('/url', (req, res) => {
  res.sendJson(201, {message: 'Booyah'});
});

app.router().post('/foo', (req, res) => {
  res.sendJson(200, req.body);
});

app.catchAll((req, res) => {
  res.sendJson(404, {message: `Route ${req.method} ${req.url} Not Found`});
});

