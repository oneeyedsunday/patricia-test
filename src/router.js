'use strict';

/**
 *
 * @param {(path: string, method: string, func: (req, res) => void)} handlerAccessorFunc
 * @returns
 */
module.exports = (handlerAccessorFunc) => ({
  /**
     *
     * @param {string} reqPath
     * @param {(req, res) => void} reqHandler
     * @returns
     */
  get(reqPath, reqHandler) {
    handlerAccessorFunc(reqPath, 'GET', reqHandler);
    return this;
  },
  /**
     *
     * @param {string} reqPath
     * @param {(req, res) => void} reqHandler
     * @returns
     */
  post(reqPath, reqHandler) {
    handlerAccessorFunc(reqPath, 'POST', reqHandler);
    return this;
  },
  /**
     *
     * @param {string} reqPath
     * @param {(req, res) => void} reqHandler
     * @returns
     */
  head(reqPath, reqHandler) {
    handlerAccessorFunc(reqPath, 'HEAD', reqHandler);
    return this;
  },
  /**
     *
     * @param {string} reqPath
     * @param {(req, res) => void} reqHandler
     * @returns
     */
  delete(reqPath, reqHandler) {
    handlerAccessorFunc(reqPath, 'DELETE', reqHandler);
    return this;
  },
  /**
     *
     * @param {string} reqPath
     * @param {(req, res) => void} reqHandler
     * @returns
     */
  put(reqPath, reqHandler) {
    handlerAccessorFunc(reqPath, 'PUT', reqHandler);
    return this;
  }
});
