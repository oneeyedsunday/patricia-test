'use strict';

const RouteNotFound = new Error('Route Not Handled');
RouteNotFound.name = 'RouteNotFound';

const errors = {
  RouteNotFound
};

module.exports = errors;
