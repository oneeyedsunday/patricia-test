'use strict';

/**
 *
 * @param {import('http').ServerResponse} res
 */
function makeFrameworkResponse(res) {
  res.sendJson = function(statusCode, message = {}) {
    res.statusCode = statusCode;
    res.write(JSON.stringify(message));
    res.end();
  };

  return res;
}

module.exports = makeFrameworkResponse;
