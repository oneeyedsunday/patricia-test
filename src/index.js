'use strict';

const http = require('http');
const FrameworkErrors = require('./error');
const makeFrameworkResponse = require('./response');
const { json } = require('body-parser');
const { promisify } = require('util');

function delayedPromise(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

const defaultLogger = {
  info: console.log,
  debug: console.log,
  error: console.error
};

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}


/**
 *
 * @param {Object} options
 * @param {number} options.port
 * @param {{ info: (...args) => void, debug: (...args) => void , error: (...args) => void}} options.logger
 * @returns
 */
function miniFramework(options = {}) {
  const port = normalizePort(options.port || 0);
  const _server = _bootstrapHttpServer(port);
  const _httpHandlers = {};
  const _middlewares = [
    (_, res, next) => {
      res.setHeader('Content-Type', 'application/json');
      res.setHeader('X-Powered-By', 'Mini Framework');
      next();
    },
    (req, res, next) => json()(req, res, next)
  ];
  const _errHandlers = [
    (err, req, res, next) => {
      if (err === FrameworkErrors.RouteNotFound) {
        _logger.error(FrameworkErrors.RouteNotFound.name, err.message, req.url);
        res.sendJson(404, { message: 'Not Found' });
      } else {
        next();
      }
    }
  ];
  const _logger = options.logger || defaultLogger;

  function applyMiddlewares(req, res, index = 0) {
    if (_middlewares[index]) {
      _middlewares[index](req, res, () => applyMiddlewares(req, res, index + 1));
    }
  }

  function applyErrorHandlers(error, req, res, index = 0) {
    if (_errHandlers[index]) {
      _errHandlers[index](error, req, res, () => applyErrorHandlers(error, req, res, index + 1));
    }
  }

  function _bootstrapHttpServer(_port) {
    return http.createServer(async(req, res) => {
      try {
        res = makeFrameworkResponse(res);
        applyMiddlewares(req, res);
        if (['PUT', 'PATCH', 'POST'].includes(req.method))
          await delayedPromise(req.headers['content-length'] || 50);
        const handler = (_httpHandlers[req.url] || {})[req.method];

        if (handler) {
          return handler(req, res);
        }

        throw FrameworkErrors.RouteNotFound;
      } catch (error) {
        applyErrorHandlers(error, req, res);
      }
    }).listen(_port, () => {
      const { address, port, family } = _server.address();
      _logger.info(`Server up and listening on ${family} ${address}/${port}`);
    });
  }

  function _requestHandlersAdder(reqPath, reqMethod, reqHandler) {
    if (!_httpHandlers[reqPath]) _httpHandlers[reqPath] = {};
    _httpHandlers[reqPath][reqMethod] = reqHandler;
  }

  _server.on('error', err => {
    _logger.error('Server Error: ' + err.message, { error: err });
    if (err.syscall !== 'listen') {
      throw err;
    }

    const bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (err.code) {
      case 'EACCES':
        _logger.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        _logger.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw err;
    }
  });
  return {
    applyMiddleware(middlewareFunc) {
      _middlewares.push(middlewareFunc);
    },

    applyErrorHandler(errorHandlerFunc) {
      _errHandlers.push(errorHandlerFunc);
    },

    router() {
      return require('./router')(_requestHandlersAdder);
    },

    /**
         *
         * @param {(req, res) => void} handlerFunc
         */
    catchAll(handlerFunc) {
      function wrapped404Handler(error, req, res, next) {
        if (error === FrameworkErrors.RouteNotFound) {
          handlerFunc(req, res);
        } else {
          next();
        }
      }
      _errHandlers[0] = wrapped404Handler;
    },

    shutdown(cb) {
      return _server.close(cb);
    },

    shutdownAsync() {
      return promisify(_server.close.bind(_server))();
    },

    get serverHandle() {
      return _server;
    }
  };
}

module.exports = miniFramework;
