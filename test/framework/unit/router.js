'use strict';

const { expect } = require('chai');
const Router = require('../../../src/router');
const expectedMethods = [
  'get', 'put', 'head', 'post', 'delete'
];

describe('Framework::Router Unit Tests', () => {
  it('Only exposes expected http methods', () => {
    const router = Router((path, method, handler) => {});
    expect(router).to.be.an('object');
    expect(Object.keys(router).length).to.eql(5);
    expect(Object.keys(router)).to.have.members(expectedMethods);
  });

  it('Methods return a chained router', () => {
    const router = Router((path, method, handler) => {});
    for (const method in router) {
      if (Object.hasOwnProperty.call(router, method)) {
        expect(router[method]()).to.equal(router);
      }
    }
  });

  it('Updates Injected accessor', () => {
    const _dataStructure = {};
    const router = Router((path, method, handler) => {
      if (!_dataStructure[path]) _dataStructure[path] = {};
      _dataStructure[path][method] = handler;
    });

    function bar() {}

    router.get('/foo', bar);

    expect(_dataStructure['/foo']['GET']).to.equal(bar);
  });
});