'use strict';

const { expect } = require('chai');
const sinon = require('sinon');
const Response = require('../../../src/response');

describe('Framework::Response Unit Tests', () => {
  /**
   * @type {import('sinon').SinonSandbox}
   */
    let sandbox;

    before(() => {
      sandbox = sinon.createSandbox();
    });

    after(() => {
      sandbox.restore();
    });

    it('should mock correctly', function () {
      // from https://stackoverflow.com/a/33698087/5134605
      const serverResponse = {
        statusCode: 0,
        writeHead: function () {
        },
        write: function () {
        },
        end: function () {
        }
      }
      const mockServerResponse = sinon.mock(serverResponse, 'writeHead');
      mockServerResponse.expects('write').once();
      mockServerResponse.expects('end').once();
      const payload  = { message: "Successful" };
      const res = Response(serverResponse);
      expect(res).to.have.property('sendJson').to.be.a('function');
      res.sendJson(200, payload);
      expect(res.statusCode).to.eql(200);
      mockServerResponse.expects('write').calledOnceWithExactly(JSON.stringify(payload));
    });
});
