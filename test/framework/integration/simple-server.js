'use strict';

const { expect } = require('chai');
const supertest = require('supertest');

const Framework = require('../../../src');


describe('Framework::Integration Tests', () => {
  describe('Simple Server', () => {

    const app = Framework({ port: 0 });

    app.router()
    .get('/foo', (req, res) => {
        res.sendJson(200, { message: "Request Successful", data: [] });
    })
    .post('/foo', (req, res) => {
      res.sendJson(202, { message: "Your Request is being processed" });
    })
    .post('/bar', (req, res) => {
        res.sendJson(201, { message: "Received successfully", data: [1, 2, 3] });
    });

    const request = supertest(app.serverHandle);
  
    after(() => {
      return app.shutdownAsync()
      .then(() => {
        console.info('Server shut down.');
      });
    });
  
    it('GET /foo works', () => {
      return request
        .get('/foo')
        .set('Accept', 'application/json')
        .expect(200)
        .then(res => {
          const { body: data } = res;
          expect(data).to.be.an('object');
          expect(data).to.have.property('message').to.be.a('string').to.eql("Request Successful");
          expect(data).to.have.property('data').to.be.an('array').to.have.members([]);
        });
    });
  
    it('POST /foo works', () => {
      return request
        .post('/foo')
        .set('Accept', 'application/json')
        .send({ foo: 'bar' })
        .expect(202)
        .then(res => {
          const { body: data } = res;
          expect(data).to.be.an('object');
          expect(data).to.have.property('message').to.be.a('string').to.eql("Your Request is being processed");
        });
    });
  
    it('POST /bar works', () => {
      return request
        .post('/bar')
        .set('Accept', 'application/json')
        .send({ foo: 'bar' })
        .expect(201)
        .then(res => {
          const { body: data } = res;
          expect(data).to.be.an('object');
          expect(data).to.have.property('message').to.be.a('string').to.eql("Received successfully");
          expect(data).to.have.property('data').to.be.an('array').to.have.members([1, 2, 3]);
        });
    });
  });

  describe('Middlewares', () => {
    const app = Framework({ port: 0 });

    const headerKey = 'X-Custom-Header';
    const headerValue = 'Idiakose Sunday';

    app.router()
    .get('/foo', (req, res) => {
        res.sendJson(200, { message: "Request Successful", data: [] });
    });

    app.applyMiddleware((req, res, next) => {
      res.setHeader(headerKey, headerValue);
      next();
    });

    const request = supertest(app.serverHandle);
  
    after((done) => {
      app.shutdown((err) => {
        if (err) throw new Error(err);
        console.info('Server shut down.');
        done();
      });
    });

    it('Middleware works', () => {
      return request
        .get('/foo')
        .set('Accept', 'application/json')
        .expect(200)
        .then(res => {
          expect(res.headers).to.have.property(headerKey.toLowerCase()).to.eql(headerValue);
        });
    });

  });

  describe('Error Handlers', () => {
    const app = Framework({ port: 0 });
    const customErrMsg = 'Yada Yada';

    app.router()
    .get('/foo', (req, res) => {
        throw new Error('Yikes');
        res.sendJson(200, { message: "Request Successful", data: [] });
    });

    app.applyErrorHandler((err, req, res, next) => {
      res.sendJson(500, { message: customErrMsg, error: err.message });
    });

    const request = supertest(app.serverHandle);
  
    after((done) => {
      app.shutdown((err) => {
        if (err) throw new Error(err);
        console.info('Server shut down.');
        done();
      });
    });

    it('Uses error handler', () => {
      return request
        .get('/foo')
        .set('Accept', 'application/json')
        .expect(500)
        .then(res => {
          const { body: data } = res;
          expect(data).to.be.an('object');
          expect(data).to.have.property('message').to.be.a('string').to.eql(customErrMsg);
          expect(data).to.have.property('error').to.be.a('string').to.eql('Yikes');
        });
    });
  });

  describe('Has default 404 Handler', () => {
    const app = Framework({ port: 0 });

    const request = supertest(app.serverHandle);
  
    after((done) => {
      app.shutdown((err) => {
        if (err) throw new Error(err);
        console.info('Server shut down.');
        done();
      });
    });

    it('Uses default 404 Handler', () => {
      return request
        .get('/foo')
        .set('Accept', 'application/json')
        .expect(404)
        .then(res => {
          const { body: data } = res;
          expect(data).to.be.an('object');
          expect(data).to.have.property('message').to.be.a('string').to.eql("Not Found");
        });
    });
  });

  describe('Can Overwrite 404 Handler', () => {
    const app = Framework({ port: 0 });

    app.catchAll((req, res) => {
      res.sendJson(404, {message: `Route ${req.method} ${req.url} Not Found`});
    });

    const request = supertest(app.serverHandle);
  
    after((done) => {
      app.shutdown((err) => {
        if (err) throw new Error(err);
        console.info('Server shut down.');
        done();
      });
    });

    it('Uses custom 404 Handler', () => {
      return request
        .get('/foo')
        .set('Accept', 'application/json')
        .expect(404)
        .then(res => {
          const { body: data } = res;
          expect(data).to.be.an('object');
          expect(data).to.have.property('message').to.be.a('string').to.eql("Route GET /foo Not Found");
        });
    });
  });
});
