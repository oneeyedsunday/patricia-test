### Architecture of Generic NodeJs Routing Framework

At the end of the day, routing is supplying a handler for a combination of http METHOD and path.

> We assume this is a http framework, and not RPC or GraphQL for example.

#### Decisions
##### How high level or otherwise should this framework be
This framework would be highlevel, consumers will simply be required to instantiate an instance of the framework and get the http goodies. The user is required to specify a port. The user is also required to specify handlers.
The framework should also allow for user supplied middlewares.
The framework should also expose support for easy to use routing.
We are a json only framework