Hello there, this is a demo app creating a http framework that is used to run a server serving some [routes](https://www.notion.so/Backend-Engineer-Role-72e0e53222174dcf9605e2c5a5b742d8).


The demo auth api server is [here](samples/auth-api)

The framework itself is [here](src)

### Sample API Calls
Register a user

```sh
curl -X POST -H "Content-Type: application/json" -d '{ "email": "isaiah@example.com", "password": "newton", "first_name": "foo", "last_name": "bar" }' http://localhost:8002/api/v1/auth/register
```

Login 

```sh
curl -X POST -H "Content-Type: application/json" -d '{ "email": "isaiah@example.com", "password": "newton" }' http://localhost:8002/api/v1/auth/login
```


Logout
```sh
curl -X POST -H "Content-Type: application/json" -H "Authorization: $TOKEN" http://localhost:8002/api/v1/auth/logout
```
